# Ceres概述

Ceres solver 是谷歌开发的一款用于非线性优化的库，在谷歌的开源激光雷达slam项目cartographer中被大量使用。Ceres Solver是谷歌2010就开始用于解决优化问题的C++库，2014年开源．在Google地图，Tango项目，以及著名的SLAM系统OKVIS和Cartographer的优化模块中均使用了Ceres Solver.

# Ceres教程

### 四个入门例子

[第一个](https://blog.csdn.net/liu502617169/article/details/88693442)

[第二个](https://blog.csdn.net/liu502617169/article/details/88845151)

[第三个]()

[第四个]()



# 参考资料

[官方文档](http://ceres-solver.org/features.html)