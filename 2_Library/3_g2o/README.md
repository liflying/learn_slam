## g2o



[g2o学习笔记](https://www.jianshu.com/p/e16ffb5b265d)

[从零开始一起学习SLAM | 理解图优化，一步步带你看懂g2o代码](https://mp.weixin.qq.com/s/j9h9lT14jCu-VvEPHQhtBw)

[从零开始一起学习SLAM | 掌握g2o顶点编程套路](https://mp.weixin.qq.com/s/12V8iloLwVRPahE36OIPcw)

[从零开始一起学习SLAM | 掌握g2o边的代码套路](https://blog.csdn.net/electech6/article/details/88539467)





## 语法问题

**static_cast< new_type >(expression)**

**static_cast**相当于传统的C语言里的强制转换，该运算符把expression转换为new_type类型，用来强迫隐式转换如non-const对象转为const对象，编译时检查，用于非多态的转换，可以转换指针及其他，**但没有运行时类型检查来保证转换的安全性**

