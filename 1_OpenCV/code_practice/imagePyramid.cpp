#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
	
	Mat dst, src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";
	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	//上采样
	/*pyrUp(src,dst,Size(src.cols*2,src.rows*2));
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(outputTitle, dst);*/

	//降采样
	pyrDown(src, dst, Size(src.cols / 2, src.rows / 2));
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(outputTitle, dst);
	//高斯不同
	Mat gray_src, g1, g2, dogImg;
	cvtColor(src,gray_src,CV_BGR2GRAY);
	GaussianBlur(gray_src, g1, Size(3, 3), 0, 0);
	GaussianBlur(g1, g2, Size(3, 3), 0, 0);
	subtract(g1,g2,dogImg,Mat());
	normalize(dogImg, dogImg,255,0,NORM_MINMAX);

	imshow("DOG Image",dogImg);

	waitKey(0);
	return 0;
}

