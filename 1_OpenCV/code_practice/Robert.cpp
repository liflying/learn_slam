#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main(int argc, char **argv) {
	Mat dst, src, gray_src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";

	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	/*//Robert x 方向
	Mat kernelX = (Mat_<char>(2, 2) << 1, 0, 0, -1);
	filter2D(src, dst, -1, kernelX, Point(-1, -1), 0.0);
	imshow("kernelX", dst);

	//Robert y 方向
	Mat kernelY = (Mat_<char>(2, 2) << 0, 1, -1, 0);
	Mat dstY;
	filter2D(src, dstY, -1, kernelY, Point(-1, -1), 0.0);
	imshow("kernelY", dstY);*/

	/*Mat kernelX = (Mat_<char>(3, 3) << -1, 0, 1, -2, 0, 2, -1, 0, 1);
	filter2D(src, dst, -1, kernelX, Point(-1, -1), 0.0);
	imshow("sobelX", dst);

	//sobel y 方向
	Mat kernelY = (Mat_<char>(3, 3) << -1, -2, -1, 0, 0, 0, 1, 2, 1);
	Mat dstY;
	filter2D(src, dstY, -1, kernelY, Point(-1, -1), 0.0);
	imshow("sobelY", dstY);*/
	//拉普拉斯算子

	Mat kernel = (Mat_<char>(3, 3) << 0, -1, 0, -1, 4, -1, 0, -1, 0);
	filter2D(src, dst, -1, kernel, Point(-1, -1), 0.0);
	imshow("la", dst);

	waitKey(0);
	return 0;
}
